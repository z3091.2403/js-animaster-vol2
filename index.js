(function main() {
  document.getElementById('fadeInPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeInBlock');
      animaster().fadeIn(block, 5000);
    });

  document.getElementById('fadeOutPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeOutBlock');
      animaster().fadeOut(block, 5000);
    });

  document.getElementById('moveAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('moveAndHideBlock');
      animaster().moveAndHide(block, 1000, { x: 100, y: 20 });
    });

  document.getElementById('moveAndHideReset')
    .addEventListener('click', function () {
      const block = document.getElementById('moveAndHideBlock');
      animaster().resetMoveAndHide(block);
    });

  document.getElementById('movePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('moveBlock');
      // animaster().move(block, 1000, { x: 100, y: 10 });

      const customAnimation = animaster()
        .addMove(200, { x: 40, y: 40 })
        .addScale(800, 1.3)
        .addMove(200, { x: 80, y: 0 })
        .addScale(800, 1)
        .addMove(200, { x: 40, y: -40 })
        .addScale(800, 0.7)
        .addMove(200, { x: 0, y: 0 })
        .addScale(800, 1);

      customAnimation.play(block);

    });

  document.getElementById('scalePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('scaleBlock');
      animaster().scale(block, 1000, 1.25);
    });

  document.getElementById('showAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('showAndHideBlock');
      animaster().showAndHide(block, 5000);
    });

  let heartBeatingObj;

  document.getElementById('heartBeatingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('heartBeatingBlock');
      heartBeatingObj = animaster().heartBeating(block, 1000);
    });

  document.getElementById('heartBeatingStop')
    .addEventListener('click', function () {
      heartBeatingObj.stop();
    });

  let shakingObj;

  document.getElementById('shakingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('shakingBlock');
      shakingObj = animaster().shaking(block);
    });

  document.getElementById('shakingStop')
    .addEventListener('click', function () {
      shakingObj.stop();
    });
})();

function animaster() {
  function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
      result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
      result.push(`scale(${ratio})`);
    }
    return result.join(' ');
  }
  function resetFadeIn(element) {
    element.classList.remove('show');
    element.classList.add('hide');
    element.style.transitionDuration = null;
  }
  function resetFadeOut(element) {
    element.classList.remove('hide');
    element.classList.add('show');
    element.style.transitionDuration = null;
  }
  function resetMoveAndScale(element) {
    element.style.transitionDuration = null;
    element.style.transform = null;
  }

  let _steps = [];
  let timeouts = 0;

  return {
    _steps: _steps,
    fadeIn:
      function move(element, duration) {
        this.addFadeIn(duration);
        this.play(element);
      },
    fadeOut:
      function fadeOut(element, duration) {
        this.addFadeOut(duration);
        this.play(element);
      },
    move:
      function move(element, duration, translation) {
        this.addMove(duration, translation);
        this.play(element);
      },
    scale:
      function scale(element, duration, ratio) {
        this.addScale(duration, ratio);
        this.play(element);
      },
    showAndHide:
      function showAndHide(element, duration) {
        this.addFadeIn(duration / 3);
        this.addDelay(duration / 3);
        this.addFadeOut(duration / 3);
        this.play(element);
      },
    moveAndHide:
      function moveAndHide(element, duration, translation) {
        this.addMove(duration * 2 / 5, translation);
        this.addFadeOut(duration * 3 / 5);
        this.play(element);
      },
    heartBeating:
      function heartBeating(element, duration) {

        this.addScale(duration / 2, 1.4);
        this.addScale(duration / 2, 1);
        return this.play(element, true);
      },
    shaking:
      function shaking(element) {
        let timer = setInterval(() => { shakingMove(element) }, 500);
        function shakingMove(element) {
          animaster().move(element, 250, { x: 20, y: 0 });
          setTimeout(() => { animaster().move(element, 250, { x: 0, y: 0 }) }, 250);
        }
        return {
          stop() {
            clearInterval(timer);
          }
        }
      },
    resetMoveAndHide:
      function resetMoveAndHide(element) {
        resetFadeOut(element);
        resetMoveAndScale(element);
      },
    addMove:
      function (duration, { x, y }) {
        let moveObj = {
          operation: 'move',
          duration: duration,
          coord: { x, y }
        }
        this._steps.push(moveObj);
        return this;
      },
    addScale:
      function (duration, ratio) {
        let stepObj = {
          operation: 'scale',
          duration: duration,
          optionParam: ratio
        }
        this._steps.push(stepObj);
        return this;
      },
    addFadeOut:
      function (duration) {
        let stepObj = {
          operation: 'fadeOut',
          duration: duration,
        }
        this._steps.push(stepObj);
      },
    addFadeIn:
      function (duration) {
        let fadeInObj = {
          operation: 'fadeIn',
          duration: duration,
        }
        this._steps.push(fadeInObj);
        return this;
      },
    addDelay: 
      function (duration) {
        let stepObj = {
          operation: 'delay',
          duration: duration,
        }
        this._steps.push(stepObj);
        return this;
      },
    play:
    
      function (element, cycled) {
        let elemCoord = null;
        let elemRatio = null;
        for (let i = 0; i < this._steps.length; i++) {
          console.log(timeouts);
          switch (this._steps[i].operation) {
            case 'move':
              setTimeout(() => {
                element.style.transitionDuration = `${this._steps[i].duration}ms`;
                element.style.transform = getTransform(this._steps[i].coord, elemRatio);
                elemCoord = this._steps[i].coord;
              }, timeouts);
              break;
            case 'fadeIn':
              setTimeout(() => {
                element.style.transitionDuration = `${this._steps[i].duration}ms`;
                element.classList.remove('hide');
                element.classList.add('show');
              }, timeouts);
              break;
            case 'scale':
              setTimeout(() => {
                element.style.transitionDuration = `${this._steps[i].duration}ms`;
                element.style.transform = getTransform(elemCoord, this._steps[i].optionParam);
                console.log(this._steps[i].optionParam)
                elemRatio = this._steps[i].optionParam;
              }, timeouts);
              break;
            case 'fadeOut':
              setTimeout(() => {
                element.style.transitionDuration = `${this._steps[i].duration}ms`;
                element.classList.remove('show');
                element.classList.add('hide');
              }, timeouts);
              break;
            case 'delay':
              setTimeout(() => {
                element.style.transitionDuration = `${this._steps[i].duration}ms`;
              }, timeouts);
              break;  
          }
          timeouts += this._steps[i].duration;
          
          if (cycled) {
            setTimeout(() => this.play(element, cycled), timeouts);
          }
  
        }
        return {
          stop: () => {
            cycled = false;
          }

        }
      }
  };
}